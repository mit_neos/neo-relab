import colorsys
import psycopg2.extras
import matplotlib.pyplot as plt
from matplotlib.patches import Patch
import numpy
import click
from matplotlib import cm

NEO_RELAB_DB = 'postgres://bkgbyrlkylzzba:5d515de5ad982d92b7c9ffdf025974822617c1b7cab14110c1a7c162e5f9b98b@ec2-52-71-153-228.compute-1.amazonaws.com:5432/d8i9lq934pv0va'

asteroid_complexes = {'S': ('Q', 'Sq', 'S', 'Sr', 'Sv'),
                      'C': ('B', 'C', 'Cb', 'Ch'),
                      'X': ('X', 'Xc', 'Xe', 'Xk', 'Xn'),
                      'Other': ('D', 'L', 'A', 'K', 'V')}
type_positions = {'Q': 1, 'Sq': 2, 'S': 3, 'Sr': 4, 'Sv': 5,
                  'B': 5, 'C': 6, 'Cb': 7, 'Ch': 8,
                  'X': 11, 'Xc': 12, 'Xe': 13, 'Xk': 14, 'Xn': 15,
                  'D': 16, 'L': 17, 'A': 18, 'K': 19, "V": 20}

type_def = {'Carbonaceous Chondrite': 'CC', 'Iron': 'I', 'Achondrite': 'AC',
            'Ordinary Chondrite': 'OC', 'Enstatite Chondrite': 'EC', 'Stony Iron': 'SI',
            'R Chondrite': 'RC', 'Enstatite Achondrite': 'EA', 'Primitive Achondrite': 'PA', 'Impact Melt': 'IM'}

subtype_abbreviations = {
    'Ungrouped OC': 'UOC', 'Other CC': 'OCC', 'Acapulcoite-Lodranite': 'AL', 'Almahata Sitta': 'AS', 'Aubrite': 'Ab',
    'Brachinite': 'Br', 'Diogenite': 'Di', 'Eucrite': 'Eu', 'Howardite': 'Ho', 'Other AC': 'OAC', 'Urelite': 'Ur',
    'Coarse Octahedrite': 'COct', 'Coarse Octahedrite IAB': 'COct IAB', 'Finest Octahedrite': 'FOct',
    'Medium Octahedrite IIIAB': 'MOct IIIAB', 'Mesosiderite': 'Me', 'Pallasite': 'Pa', 'Unique SI': 'USI',
    'Other I': 'OI'
}


# get consistent colors across all figures
all_colors = list(cm.get_cmap('tab10').colors)
all_colors.append((1, 0.25, 1))
type_colors = dict(zip(type_def.keys(), all_colors))

# ghost_bars = {'B': ['CC'], 'C': ['CC'], 'Cb': ['CC'], 'A': ['CC'], 'Sa': ['CC'], 'A/Sa': ['CC'], 'D': ['I']}

plt.rcParams["font.family"] = "sans-serif"
plt.rcParams['font.sans-serif'] = 'Helvetica'


@click.command()
@click.argument('table', nargs=1, type=str)
@click.option('--refresh', default=False, is_flag=True)
def run(table, refresh):
    plot_maker(query(table=table, refresh=refresh), table)


def query_sql():
    return """
    --Query 1
--meteorite subtypes per asteroid type
SELECT n_sample_bymettype,
      nsample,
      ROUND((n_sample_bymettype::numeric / nsample::numeric) * 100, 1)      AS sample_frac,
      n_met_bymettype,
      nspec,
      ROUND((n_met_bymettype::numeric / nspec::numeric) * 100, 1)           AS met_frac,
       n_ast_bymettype,
       n_ast_byasttype,
       ROUND((n_ast_bymettype::numeric / n_ast_byasttype::numeric) * 100, 1) AS ast_match,        --91.7% of Btypes match CCs, 37% match OCs - all matching to a specific 4 spectra {H3-4:4}
       f.type AS ast_type,
       f.source,
       f.generaltype1,
       f.type1 AS met_type,
      JSON_OBJECT_AGG(subtype, n_matches)                                   AS n_matches,
      JSON_OBJECT_AGG(subtype, n_distinctspectra)                           AS n_distinctspectra,
       JSON_OBJECT_AGG(subtype, n_distinctsamples)                           AS subclasses -- n_distinctsamples --NOW PLOT WITH THIS SET
FROM (SELECT COUNT(*)                             AS n_matches,
             COUNT(DISTINCT m.spectrumid)         AS n_distinctspectra,
             COUNT(DISTINCT LEFT(m.sampleid, 10)) AS n_distinctsamples,
             m.source,
             m.generaltype1,
             m.type1,
             m.subtype,
             m.type,
             n.nspec,
             n.nsample
      FROM neo_relab.temp_final_data_samples m
               LEFT OUTER JOIN neo_relab.nspec_particulate n
                               ON n.source = m.source AND n.generaltype1 = m.generaltype1 AND n.type1 = m.type1 AND
                                  n.subtype = m.subtype
      WHERE type IS NOT NULL
        AND type NOT ILIKE '%:%' -- and m.source='Other-Met' and m.generaltype1='Rock'
        AND goodfit
	  and particulate='Yes' 
      GROUP BY m.source, m.generaltype1, m.type1, m.type, m.subtype, n.nspec, n.nsample
      ORDER BY m.source, type, generaltype1, type1, n_matches DESC --meteorites per asteroid type
     ) f
         LEFT OUTER JOIN (SELECT type, COUNT(DISTINCT m.asteroidfilename) AS n_ast_byasttype
                          FROM neo_relab.temp_final_data_samples m
                          WHERE type IS NOT NULL
                          --AND goodfit
                          GROUP BY type) g ON g.type = f.type
         LEFT OUTER JOIN (SELECT type,
                                 m.generaltype1,
                                 m.source,
                                 m.type1,
                                 COUNT(DISTINCT m.asteroidfilename) AS n_ast_bymettype,
                                 COUNT(DISTINCT m.spectrumid)       AS n_met_bymettype,
                                 COUNT(DISTINCT m.sampleid)         AS n_sample_bymettype
                          FROM neo_relab.temp_final_data_samples m
                          WHERE type IS NOT NULL
                          AND goodfit
						  and particulate='Yes' 
                          GROUP BY m.source, m.generaltype1, type, m.type1) h
                         ON h.source = f.source AND h.generaltype1 = f.generaltype1 AND h.type = f.type AND
                            h.type1 = f.type1
WHERE f.source = 'Other-Met'
  AND f.generaltype1 = 'Rock'
  AND f.type1 IS NOT NULL
  AND f.subtype IS NOT NULL
GROUP BY f.type, f.source, f.generaltype1, f.type1, nspec, nsample, n_met_bymettype, n_ast_bymettype, n_ast_byasttype,
         n_sample_bymettype
ORDER BY f.type, n_ast_bymettype DESC NULLS LAST;"""


def query(table: str = 'results_other_met', refresh: bool = False):
    # grab the data

    print(f'Running fn_final_data() for table [{table}]')
    connection = psycopg2.connect(NEO_RELAB_DB)
    cur = connection.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)

    if refresh:
        print('Refreshing data table...')
        cur.execute(f"SELECT * FROM neo_relab.fn_final_data('neo_relab.{table}', 30)")
        connection.commit()

    print('Retrieving data...')
    cur.execute(query_sql()),
    data = cur.fetchall()
    cur.close()
    connection.close()

    return data


def get_complex(data: list, complex_allowables: tuple):
    # filter by asteroid complex
    return [record for record in data if record.ast_type in complex_allowables]


def get_asteroid_type(data: list, asteroid_type: str):
    # filter by asteroid type
    return [record for record in data if record.ast_type == asteroid_type]


def scale_lightness(rgb, scale_l=1):
    # convert rgb to hls
    h, l, s = colorsys.rgb_to_hls(*rgb)
    # manipulate h, l, s values and return as rgb
    return colorsys.hls_to_rgb(h, min(1, l * scale_l), s=s)


def color_maker(data, meteorites):
    """
    We want each meteor to have a determined color and each
    subtype of that meteor to have a fixed shad of that color
    {'met': {'color': '', 'subtypes': {'<subtype>': '<color>'}}}
    """

    subtype_colors = {}
    for met in meteorites:
        met_color = type_colors.get(met)

        subtypes = []
        subtype_colors[met] = {'color': met_color, 'subtypes': None}
        subtypes_fields = [d.subclasses for d in data if d.met_type == met]

        # now get all the keys of the subtypes fields
        for subtypes_field in subtypes_fields:
            if 'Other' in subtypes_field.keys():
                subtypes_field[f'Other ({met})'] = subtypes_field.pop('Other')

            if 'other' in subtypes_field.keys():
                subtypes_field[f'Other ({met})'] = subtypes_field.pop('other')

            if 'Unique' in subtypes_field.keys():
                subtypes_field[f'Unique ({met})'] = subtypes_field.pop('Unique')

            subtypes.extend(list(subtypes_field.keys()))

        # distinct the subtypes
        subtypes = sorted(set(subtypes))
        scale_list = numpy.linspace(1, 0.5, len(subtypes))
        this_subtype_colors = dict(zip(subtypes, [scale_lightness(met_color, s) for s in scale_list]))

        subtype_colors[met]['subtypes'] = this_subtype_colors

    return subtype_colors


def subplot_maker(fig_ax, data, meteorite_positions, colors):
    """ Subplot maker (one subplot per asteroid type)"""

    # get all the meteorite types for this asteroid type
    unique_met_types = sorted(set([d.met_type for d in data]))

    # each met is a bar on the axis
    for met in unique_met_types:
        x_value = meteorite_positions.get(met)
        asteroid_for_meteorite = [d for d in data if d.met_type == met][0]

        # now, for each bar, we need to break the bar into subtypes
        bottom = 0
        subtype_total = sum(asteroid_for_meteorite.subclasses.values())

        for s, (subtype, count) in enumerate(
                sorted(asteroid_for_meteorite.subclasses.items(), key=lambda x: x[1], reverse=True)):

            # the height of this subtype
            height_of_subtype = (count / subtype_total) * float(asteroid_for_meteorite.ast_match)

            if subtype == 'Other':
                subtype = f'Other ({met})'
            if subtype == 'Unique':
                subtype = f'Unique ({met})'

            edgecolor = 'white'
            color = colors[met]['subtypes'].get(subtype)
            text_color = 'white'
            # try:
            #     if type_def.get(met) in ghost_bars.get(ast_type):
            #         transparent_value = 0.4
            #         edgecolor = color
            #         color = 'white'
            #         text_color = 'black'
            # except:
            #     pass

            # add the subtype height to the plot
            fig_ax.bar(x_value, height_of_subtype,
                       bottom=bottom,
                       color=color,
                       edgecolor=edgecolor,
                       linewidth=1)

            if height_of_subtype > 10:
                subtype_text = subtype_abbreviations.get(subtype, subtype)
                # subtype_text = '\n'.join([st.capitalize() if len(st) > 2 else st.upper() for st in subtype.split()])
                # if 'Hed' == subtype_text.split()[0]:
                #     subtype_text = subtype_text.split()[0].upper() + '\n' + ' '.join(subtype_text.split()[1:])
                fig_ax.annotate(subtype_text,
                                (x_value, bottom + (0.5 * height_of_subtype)),
                                ha='center', va='center', fontsize=8, color=text_color)

            # the next subtype section will start at the top of the previous subtype section
            bottom += height_of_subtype

    fig_ax.set_ylim([0, 100])  # all plots should show from 0 - 100 percent
    fig_ax.set_yticklabels([f'{y:.0f}%' for y in fig_ax.get_yticks()], fontsize=9)


def plot_maker(data, table_name):
    """ Figure maker """

    # fig, axes = plt.subplots(ncols=6, nrows=4, figsize=(18, 9))
    fig = plt.figure(figsize=(17, 9))

    # dummy axis for 'super'-labels
    major_axis = fig.add_subplot(111, frameon=False)
    plt.tick_params(labelcolor='none', which='both', top=False, bottom=False, left=False, right=False)

    # get all unique meteorite types
    meteorites = sorted(set([d.met_type for d in data]))
    meteorites = dict(zip(meteorites, range(len(meteorites))))

    # deteremine the colors to be used for the bars
    colors = color_maker(data, meteorites.keys())

    # each row in the figure is a complex
    for row, (complex, asteroid_types) in enumerate(asteroid_complexes.items()):
        complex_asteroids = get_complex(data, asteroid_types)
        n_cols = 4 if complex == 'C' else 5
        n_rows = 4

        # each column in the figure is a plot for the asteroid type
        for column, asteroid_type in enumerate(asteroid_types):
            type_asteroids = get_asteroid_type(complex_asteroids, asteroid_type)
            print(type_asteroids)
            try:
                n_asteroids = type_asteroids[0].n_ast_byasttype
            except:
                n_asteroids = 0

            # get the row-col subplot and set the title
            # asteroid_axis = axes[row, column]
            print(n_rows, n_cols, type_positions.get(asteroid_type))
            asteroid_axis = fig.add_subplot(n_rows, n_cols, type_positions.get(asteroid_type))

            # dummy plot to get all ticks
            asteroid_axis.bar(meteorites.values(), [0] * len(meteorites))
            asteroid_axis.set_xticks(list(range(len(meteorites))))
            asteroid_axis.set_xticklabels([type_def[u] for u in meteorites.keys()], fontsize=9)

            # asteroid_axis.set_title(f'A/Sa ({n_asteroids})' if asteroid_type == 'A' else f'{asteroid_type.capitalize()} ({n_asteroids})',
            #                         fontsize=13)
            asteroid_axis.annotate(
                f'A/Sa ({n_asteroids})' if asteroid_type == 'A' else f'{asteroid_type.capitalize()} ({n_asteroids})',
                (2.5, 90), ha='center', va='center', fontsize=13)
            # make the subplot for this asteroid type
            subplot_maker(asteroid_axis, type_asteroids, meteorites, colors)

            if column != 0:
                for spine in ['right', 'left', 'top']:
                    asteroid_axis.spines[spine].set_visible(False)
                asteroid_axis.xaxis.set_ticks_position('bottom')
                asteroid_axis.yaxis.set_ticks_position('none')
                asteroid_axis.set_yticklabels([])

            # add a ylabel/row label
            if column == 0:
                # asteroid_axis.set_ylabel(f"{complex.capitalize()}{'-Complex' if row != 3 else ''}")
                for spine in ['right', 'top']:
                    asteroid_axis.spines[spine].set_visible(False)

            # asteroid_axis.label_outer()

    # for each met type, create a legend of subtypes
    offset_from_top = 0
    n_subtypes = []
    for s in colors.values():
        n_subtypes.extend(s['subtypes'].keys())
    n_subtypes = len(n_subtypes)

    legend_anchor_ys = [4.73, 4.06, 3.24, 2.15, 1.6, 0.88, 0.05]
    met_labels_anchor_ys = [0.95, 0.78, 0.6, 0.42, 0.281, 0.16, 0.03]

    ordered_mets = ['Ordinary Chondrite', 'Carbonaceous Chondrite', 'Achondrite', 'Enstatite Chondrite', 'Iron',
                    'Stony Iron', 'R Chondrite']

    for m, met in enumerate(ordered_mets):
        met_colors = colors.get(met)

        met_label = met.split()
        major_axis.text(1.026 if len(met_label) > 1 else 1.034,
                        met_labels_anchor_ys[m], '\n'.join(met_label) + f" ({type_def[met]})",
                        ha='center', va='center', rotation='vertical',
                        color=met_colors['color'])

        subtypes_colors = met_colors.get('subtypes')
        legend_subtypes = [Patch(facecolor=color, label=f"{subtype} {'(' + subtype_abbreviations.get(subtype) + ')' if subtype_abbreviations.get(subtype) else ''}") for subtype, color in subtypes_colors.items()]

        legend_loc = 'lower left' if m == len(colors) - 1 else 'upper left'
        subtypes_legend = plt.legend(handles=legend_subtypes, loc=legend_loc, bbox_to_anchor=(1.3, legend_anchor_ys[m]),
                                     ncol=1, labelspacing=0, labelcolor=subtypes_colors.values(), borderpad=0,
                                     borderaxespad=0, frameon=False)
        plt.gca().add_artist(subtypes_legend)

        offset_from_top += len(legend_subtypes) / n_subtypes

    major_axis.set_ylabel('Cumulative % of asteroid spectra that match to each meteorite type\n',
                          fontdict={'fontsize': 14})
    plt.subplots_adjust(left=0.06, right=0.775, wspace=0.07, hspace=0.275, top=0.95, bottom=0.05)
    plt.savefig(f'grid_of_bars_{table_name}.png', dpi=400)
    plt.show()


if __name__ == "__main__":
    run()
