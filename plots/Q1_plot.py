import psycopg2
import pandas
import re
import colorsys
from tqdm import tqdm
import matplotlib.pyplot as plt
import numpy
import seaborn as sns
import os
import click

plt.switch_backend('Agg')
NEO_RELAB_DB = 'postgres://bkgbyrlkylzzba:5d515de5ad982d92b7c9ffdf025974822617c1b7cab14110c1a7c162e5f9b98b@ec2-52-71-153-228.compute-1.amazonaws.com:5432/d8i9lq934pv0va'
sns.set(style='white')
sns.set_context('paper')

type_def = {'Carbonaceous Chondrite': 'CC', 'Achondrite': 'AC', 'Ordinary Chondrite': 'OC', 'Iron': 'FE',
            'Enstatite Chondrite': 'EC', 'Stony Iron': 'SFE', 'Chondrite': 'C', 'R Chondrite': 'R', 'Enstatite Achondrite': 'EA',
            'Impact Melt': 'IM', 'Igneous': 'I', 'Primitive Achondrite': 'PA', 'Mixture': 'M', 'NO_MET_TYPE': 'NMT'}

# get consistent colors across all figures
all_colors = sns.color_palette()
all_colors.extend([(0.0, 0.0, 0.0), (1.0, 0.8431372549019608, 0.0)])
type_colors = dict(zip(type_def.keys(), all_colors))

QUERY_1 = """
--Query 1
--meteorite subtypes per asteroid type
SELECT n_sample_bymettype,
      nsample,
      ROUND((n_sample_bymettype::numeric / nsample::numeric) * 100, 1)      AS sample_frac,
      n_met_bymettype,
      nspec,
      ROUND((n_met_bymettype::numeric / nspec::numeric) * 100, 1)           AS met_frac,
       n_ast_bymettype as n_ast_match,
       n_ast_byasttype as n_ast_total,
       ROUND((n_ast_bymettype::numeric / n_ast_byasttype::numeric) * 100, 1) AS ast_match_percent,        --91.7% of Btypes match CCs, 37% match OCs - all matching to a specific 4 spectra {H3-4:4}
       f.type AS ast_type,
       f.source,
       f.generaltype1,
       f.type1 AS met_type,
      JSON_OBJECT_AGG(subtype, n_matches)                                   AS n_matches,
      JSON_OBJECT_AGG(subtype, n_distinctspectra)                           AS n_distinctspectra,
       JSON_OBJECT_AGG(subtype, n_distinctsamples)                           AS subclasses -- n_distinctsamples --NOW PLOT WITH THIS SET
FROM (SELECT COUNT(*)                             AS n_matches,
             COUNT(DISTINCT m.spectrumid)         AS n_distinctspectra,
             COUNT(DISTINCT LEFT(m.sampleid, 10)) AS n_distinctsamples,
             m.source,
             m.generaltype1,
             m.type1,
             m.subtype,
             m.type,
             n.nspec,
             n.nsample
      FROM neo_relab.temp_final_data_samples m
               LEFT OUTER JOIN neo_relab.nspec_particulate n
                               ON n.source = m.source AND n.generaltype1 = m.generaltype1 AND n.type1 = m.type1 AND
                                  n.subtype = m.subtype
      WHERE type IS NOT NULL
        AND type NOT ILIKE '%:%' -- and m.source='Other-Met' and m.generaltype1='Rock'
        AND goodfit
	  and particulate='Yes' 
      GROUP BY m.source, m.generaltype1, m.type1, m.type, m.subtype, n.nspec, n.nsample
      ORDER BY m.source, type, generaltype1, type1, n_matches DESC --meteorites per asteroid type
     ) f
         LEFT OUTER JOIN (SELECT type, COUNT(DISTINCT m.asteroidfilename) AS n_ast_byasttype
                          FROM neo_relab.temp_final_data_samples m
                          WHERE type IS NOT NULL
                          --AND goodfit
                          GROUP BY type) g ON g.type = f.type
         LEFT OUTER JOIN (SELECT type,
                                 m.generaltype1,
                                 m.source,
                                 m.type1,
                                 COUNT(DISTINCT m.asteroidfilename) AS n_ast_bymettype,
                                 COUNT(DISTINCT m.spectrumid)       AS n_met_bymettype,
                                 COUNT(DISTINCT m.sampleid)         AS n_sample_bymettype
                          FROM neo_relab.temp_final_data_samples m
                          WHERE type IS NOT NULL
                          AND goodfit
						  and particulate='Yes' 
                          GROUP BY m.source, m.generaltype1, type, m.type1) h
                         ON h.source = f.source AND h.generaltype1 = f.generaltype1 AND h.type = f.type AND
                            h.type1 = f.type1
WHERE f.source = 'Other-Met'
  AND f.generaltype1 = 'Rock'
  AND f.type1 IS NOT NULL
  AND f.subtype IS NOT NULL
GROUP BY f.type, f.source, f.generaltype1, f.type1, nspec, nsample, n_met_bymettype, n_ast_bymettype, n_ast_byasttype,
         n_sample_bymettype
ORDER BY f.type, n_ast_bymettype DESC NULLS LAST;
"""


@click.command()
@click.argument('table', nargs=1, type=str)
def run(table):

    # grab the data
    print('Querying the data...')
    print(f'Table {table}')
    connection = psycopg2.connect(NEO_RELAB_DB)
    # with connection as conn:
    #     with conn.cursor() as cur:
    #         cur.execute(f"SELECT * FROM neo_relab.fn_final_data('neo_relab.{table}', 30)")

    data = pandas.read_sql(QUERY_1, connection)
    print(data)
    connection.close()

    all_met_types = data.met_type.unique().tolist()

    print("Making bar plots...")
    # make_bar_plot(data, all_met_types, subtypes=False)
    make_bar_plot(data, all_met_types, table, subtypes=True)


def scale_lightness(rgb, scale_l=1):
    # convert rgb to hls
    h, l, s = colorsys.rgb_to_hls(*rgb)
    # manipulate h, l, s values and return as rgb
    return colorsys.hls_to_rgb(h, min(1, l * scale_l), s=s)


def make_bar_plot(data: pandas.DataFrame = None, met_types: list = None, table: str = 'results',
                  subtypes: bool = True):
    unique_types = data.ast_type.unique()

    n_types = len(unique_types)
    for t in tqdm(unique_types, total=n_types):
        type_data = data[data.ast_type == t]
        met_list = type_data.met_type.tolist()
        for m in met_types:
            if m not in met_list:
                type_data = type_data.append({'met_type': m, 'ast_match': 0, 'n_met_bymettype': 0},
                                             ignore_index=True)

        # make sure the ordering is right
        type_data.sort_values(by='met_type', inplace=True)
        # type_data.reset_index(inplace=True)

        fig, ax = plt.subplots(figsize=(10, 7))
        colors = type_data.met_type.apply(lambda x: type_colors[x])

        # break the bars into subclasses
        for x_value, val in enumerate(
                list(
                    zip(type_data.n_met_bymettype, type_data.ast_match, type_data.met_type, type_data.subclasses))):

            total_perc = val[1]
            if not total_perc:
                continue
            subclasses = val[3]
            if subtypes:
                # get the color for this bar
                x_type = type_data.met_type.tolist()[x_value]
                this_type_color = type_colors.get(x_type)

                if type(subclasses) is dict:

                    # slightly lighten/darken each subclass
                    scale_list = numpy.linspace(0.6, 1.5, len(subclasses))

                    subclasses = val[3]
                    subclasses_total = sum(subclasses.values())
                    cumulative_count = 0
                    # make a bar plot for each subclass
                    for s, (subclass, count) in enumerate(sorted(subclasses.items(), key=lambda x: x[1])):

                        subtype_color = scale_lightness(this_type_color, scale_list[s])
                        subclass_height = ((subclasses_total - cumulative_count) / subclasses_total) * total_perc
                        ax.bar(x_value, subclass_height, color=subtype_color, align='center')
                        cumulative_count += count

                        # add the subclass label to the middle of the section
                        wb_text = '#ffffff'
                        if (subtype_color[0] * 0.299 + subtype_color[1] * 0.587 + subtype_color[2] * 0.114) > .35:
                            wb_text = '#000000'

                        next_height = ((subclasses_total - cumulative_count) / subclasses_total) * total_perc
                        middle_height = subclass_height - ((subclass_height - next_height) / 2)
                        label_subclass = re.sub('\s+', '\n', subclass)
                        ax.annotate(f'{(label_subclass, "")[subclass_height - next_height <= 5]}',
                                    xy=(x_value, middle_height - 0.1), ha='center', va='center',
                                    fontsize=8, color=wb_text)

                else:
                    ax.bar(x_value, 0)

            # annotate with the counts for each type
            # add total count above bar
            ax.annotate(f'{val[0]:.0f}', xy=(x_value, total_perc + 2), ha='center', fontsize=12)

        # visual modifications
        plt.ylabel(f'Cumulative percent of {t}-type asteroid spectra that\nmatch to each meteorite type', size=13)
        plt.xlabel(None)
        plt.title(f'{t}-type', size=17)

        ax.set_ylim([0, 110])
        ax.set_yticks([i * 10 for i in range(12) if i % 2 == 0])
        ax.set_yticklabels([f'{t:.0f}%' for t in ax.get_yticks()])
        # ax.bar(type_data.met_type, type_data.ast_match, color=colors, alpha=(1, 0)[subtypes])

        # labels = type_data.met_type.apply(lambda x: x.replace(' ', '\n')).tolist()
        # ax.set_xticks(ax.get_xticks())
        # ax.set_xticklabels(labels)

        ax.tick_params(axis='y', labelsize=10, left=True, direction='in')
        ax.tick_params(axis='x', bottom=True, labelsize=8, direction='inout')
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)

        fig.tight_layout()

        savedir = f'/Users/brian/Desktop/FrancescaQueries/Q1/{table}/'
        os.makedirs(savedir, exist_ok=True)
        plt.savefig(f"{savedir}bar{('', '_subtype')[subtypes]}_plot_{t}_visnir.png", dpi=300)
        plt.close(fig)


if __name__ == '__main__':
    run()
