import pandas
import psycopg2
import numpy
import matplotlib.pyplot as plt
from matplotlib import rcParams
import multiprocessing as mp
import click
import os

plt.switch_backend('Agg')

rcParams['legend.handlelength'] = 0
NEO_RELAB_DB = 'postgres://bkgbyrlkylzzba:5d515de5ad982d92b7c9ffdf025974822617c1b7cab14110c1a7c162e5f9b98b@ec2-52-71-153-228.compute-1.amazonaws.com:5432/d8i9lq934pv0va'


@click.command()
@click.argument('table', nargs=1, type=str)
def run(table):

    print("Gathering data...")
    met_subtypes, data = gather_data(table)

    dict_data = [{'subtype': m_type,
                  'data': data[data.subtype == m_type],
                  'table': table} for m_type in met_subtypes]

    total = len(dict_data)
    done = 0
    with mp.Pool(5) as p:
        for ms in p.imap_unordered(make_plots, dict_data):
            done += 1
            print(f'{ms}: {done}/{total} done')


def gather_data(table):

    connection = psycopg2.connect(NEO_RELAB_DB)

    # cur = connection.cursor()
    # cur.execute(f"Select * from neo_relab.fn_final_data('neo_relab.{table}', 30)")
    # connection.commit()

    data = pandas.read_sql('Select * from neo_relab.temp_final_data where goodfit;', connection)
    connection.close()

    unique_subtypes = data.subtype.unique()

    return unique_subtypes, data


def _calc_fit(x, y, around=0.55):

    # get the closest wavelength to `normalize_wavelength`
    closest_index, closest_value = min(enumerate(x), key=lambda w: abs(w[1]-around))
    if abs(closest_value - around) > 0.5:
        return None

    closest_index = 10 if closest_index < 10 else closest_index

    # do a first order extrapolation if we are at the very beginning of the meteorite spectrum
    fit_order = 2 if closest_index else 1

    # fit a polynomial around the normalize wavelength (2nd degree)
    fit = numpy.poly1d(numpy.polyfit(x[closest_index - 10:closest_index + 10],
                                     y[closest_index - 10:closest_index + 10], fit_order))
    return fit


def _normalize(wavelength, reflectance, normalization_wavelength=None):

    if not normalization_wavelength:
        normalization_wavelength = 0.55 if min(wavelength) <= 0.55 else 1.215

    # calculate the value at `normalize_wavelength`
    normalization_value = _calc_fit(wavelength, reflectance, normalization_wavelength)(normalization_wavelength)
    normalized_reflectance = [r / normalization_value for r in reflectance]

    return normalized_reflectance, normalization_wavelength


def make_plots(d):

    met_subtype = d['subtype']
    data = d['data']
    unique_asteroids = data.asteroidfilename.unique()

    savedir = f"/Users/brian/Desktop/inspectionGrids_metSubtypes/{d['table']}/{met_subtype}/"
    os.makedirs(savedir, exist_ok=True)

    n_rows = 6
    n_cols = 6
    # each asteroid should be on its own row

    for u_asteroid in unique_asteroids:

        fig, axes = plt.subplots(nrows=n_rows, ncols=n_cols, sharex=True, sharey=True, figsize=(25, 15))
        legend_list = []
        legend_labels = []

        this_asteroid = data[data.asteroidfilename == u_asteroid]
        for c, match_row in enumerate(this_asteroid.itertuples()):
            row = c // n_rows
            col = c % n_cols

            asteroid = match_row.spectrum_data['asteroid']
            a_reflectance, _ = _normalize(asteroid['wavelength'], asteroid['reflectance'])
            meteorite = match_row.spectrum_data['meteorite']

            axes[row, col].scatter(asteroid['wavelength'], a_reflectance, marker='.', s=2)
            mplt = axes[row, col].scatter(meteorite['wavelength'], meteorite['reflectance_normalized'], marker='.',
                                          s=1.3, color='black')

            if col == 0:
                y_label = "\n".join(u_asteroid.replace(".txt", "").split('.'))
                axes[row, col].set_ylabel(f'{y_label}')

            axes[row, col].axis(False)
            axes[row, col].spines['right'].set_visible(False)
            axes[row, col].spines['top'].set_visible(False)
            axes[row, col].spines['left'].set_visible(False)
            axes[row, col].spines['bottom'].set_visible(False)
            axes[row, col].xaxis.set_ticks_position('none')
            axes[row, col].yaxis.set_ticks_position('none')

            axes[row, col].legend(loc='upper left', title=f'{c}', frameon=False, labels=[], prop={'size': 3})
            axes[row, col].set_xlim([0, 2.5])
            axes[row, col].set_ylim([0.5, 2])

            legend_list.append(mplt)
            legend_labels.append(f'{c}. {match_row.sampleid}: Chi-Sq={round(match_row.chisq, 4)}\n'
                                 f'Albedo={match_row.albedo_reflectance:.2f}\n'
                                 f'{match_row.source}:{match_row.generaltype1}:{match_row.type1}\n'
                                 f'{match_row.subtype}')

        fig.legend(legend_list, legend_labels, loc='center right',
                   title=f"{u_asteroid.replace('.txt', '')} ({this_asteroid.type}-Type)\n MetSubtype: {met_subtype})",
                   ncol=2, prop={'size': 7}, numpoints=None, borderpad=5, labelspacing=1, frameon=False)

        fig.tight_layout()
        fig.subplots_adjust(wspace=0.08, hspace=0.05, right=0.6)

        fig.savefig(f'{savedir}{u_asteroid}.png', dpi=300)

        for i in range(n_cols):
            for j in range(n_rows):
                axes[i, j].clear()

        plt.close(fig)   # close the figure to reclaim memory

    return met_subtype


if __name__ == "__main__":
    mp.set_start_method('forkserver')
    run()
