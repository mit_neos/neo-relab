import psycopg2.extras
import matplotlib.pyplot as plt

NEO_RELAB_DB = 'postgres://bkgbyrlkylzzba:5d515de5ad982d92b7c9ffdf025974822617c1b7cab14110c1a7c162e5f9b98b@ec2-52-71-153-228.compute-1.amazonaws.com:5432/d8i9lq934pv0va'
QUERY = """
WITH francesca_chunk AS (SELECT rank,
                                (CASE WHEN subtype = 'Medium Octahedrite IIIAB' THEN 'Iron' ELSE subtype END) AS subtype,
                                spectrumid,
                                (CASE
                                     WHEN type = 'Q' THEN 'a001862.visnir.txt'
                                     WHEN type = 'Sq' THEN 'a000003.sp21.txt'
                                     WHEN type = 'S' THEN 'a000005.sp28.txt'
                                     WHEN type = 'Sr' THEN 'a000180.sp18.txt'
                                     WHEN type = 'Xn' THEN 'a000044.sp71.txt'
                                     WHEN type = 'C' THEN 'a000010.sp28.txt'
                                     WHEN type = 'B' THEN 'a000002.sp05.txt'
                                     WHEN type = 'L' THEN 'a000387.sp22.txt'
                                     WHEN type = 'A' THEN 'a000354.sp12.txt'
                                     WHEN type = 'D' THEN 'a000072.sp259n2.txt'
                                     WHEN type = 'K' THEN 'a000742.sp21.txt'
                                     WHEN type = 'Sv' THEN 'a002965.sp40.txt'
                                     WHEN type = 'T' THEN 'a000308.sp39.txt'
                                     WHEN type = 'V' THEN 'a001904.sp01.txt'
                                     WHEN type = 'X' THEN 'a003554.visnir.txt'
                                     WHEN type = 'Xc' THEN 'a000739.sp18.txt'
                                     WHEN type = 'Xk' THEN 'a000016.sp02.txt'
                                     ELSE f.asteroidfilename END)                                             AS asteroidfilename,
                                type,
                                spectrum_data
                         FROM (
                                  -- select all where goodfit and particulate, rank by t
                                  SELECT RANK() OVER (PARTITION BY type,type1,subtype ORDER BY chisq) AS rank,
                                         type1,
                                         subtype,
                                         spectrumid,
                                         asteroidfilename,
                                         type,
                                         chisq,
                                         spectrum_data
                                  FROM neo_relab.temp_final_data
                                  WHERE goodfit
                                    AND particulate = 'Yes'
                                  ORDER BY type, type1, subtype, chisq) f
                         WHERE rank = 1
                             AND (subtype NOT IN ('Almahata Sitta', 'Ungrouped OC', 'Other CC', 'Other I'))
                             AND (type IN
                                  ('A', 'B', 'C', 'Cb', 'Ch', 'K', 'Q', 'S', 'Sq', 'Sr', 'Sv', 'T', 'X', 'Xc', 'Xe',
                                   'Xk')
                                 OR (type = 'D' AND
                                     spectrumid NOT IN ('CGP011', 'C1SC63', 'MGP015', 'CGMB47', 'CEMB47', 'C1SC63'))
                                 OR (type = 'L' AND subtype NOT IN ('Unique SI', 'CK', 'CM', 'CO', 'H', 'L', 'LL'))
                                 OR (type = 'V' AND subtype <> 'Diogenite')
                                   )
                             AND spectrumid != 'BKR1MP128'
                            OR (spectrumid = 'C1MP22' AND rank = 3)
),
     asteroid_data AS (
         SELECT *
         FROM (
                  SELECT asteroidfilename,
                         spectrum_data,
                         ROW_NUMBER() OVER (PARTITION BY asteroidfilename) AS rn
                  FROM neo_relab.temp_final_data) af
         WHERE af.rn = 1
     )
SELECT fc.subtype,
       fc.spectrumid,
       fc.asteroidfilename,
       fc.type,
       fc.spectrum_data AS met_data,
       ad.spectrum_data AS ast_data
FROM francesca_chunk fc
         JOIN asteroid_data ad ON ad.asteroidfilename = fc.asteroidfilename;
"""

type_positions = {'Q': (4, 5, 1), 'Sq': (4, 5, 2), 'S': (4, 5, 3), 'Sr': (4, 5, 4), 'Sv': (4, 5, 5),
                  'B': (4, 4, 5), 'C': (4, 4, 6), 'Cb': (4, 4, 7), 'Ch': (4, 4, 8),
                  'X': (4, 4, 9), 'Xc': (4, 4, 10), 'Xe': (4, 4, 11), 'Xk': (4, 4, 12),
                  'D': (4, 5, 16), 'L': (4, 5, 17), 'A': (4, 5, 18), 'K': (4, 5, 19), "V": (4, 5, 20)}
type_ylim = {'Q': (0.6, 1.25), 'Sq': (0.7, 1.15), 'S': (0.5, 1.3), 'Sr': (0.7, 1.2), 'Sv': (0.7, 1.55),
             'B': (0.65, 1.15), 'C': (0.8, 1.4), 'Cb': (0.9, 1.1), 'Ch': (0.8, 1.2),
             'X': (0.75, 1.75), 'Xc': (0.6, 1.6), 'Xe': (0.8, 1.3), 'Xk': (0.7, 1.6),
             'D': (0.5, 2.7), 'L': (0.4, 1.8), 'A': (0.6, 1.3), 'K': (0.6, 1.5), "V": (0.1, 1.9)}


def query():
    print('Retrieving data...')
    connection = psycopg2.connect(NEO_RELAB_DB)
    cur = connection.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)
    cur.execute(QUERY)

    data = cur.fetchall()
    cur.close()
    connection.close()

    return data


def make_subplot(figure, data, position):
    legend_object = {}
    row, col, pos = position
    ax = figure.add_subplot(row, col, pos)
    atype = data[0].type

    colors = {}

    # get the asteroid from the first row
    asteroid_data = data[0].ast_data['asteroid']
    ax.plot(
        asteroid_data['wavelength_final'],
        asteroid_data['reflectance_final'],
        color='k',
        lw=1
    )
    legend_object['asteroid'] = data[0].asteroidfilename

    # now plot each meteorite
    for i, d in enumerate(data):
        meteorite_data = data[i].met_data['meteorite']
        label = f'{d.subtype}'
        p = ax.plot(
            meteorite_data['wavelength_final'],
            meteorite_data['reflectance_final'],
            lw=0.8,
            alpha=0.8,
            label=label
        )
        colors[label] = p[0].get_color()

    leg = ax.legend(
        loc='lower right',
        ncol=2,
        fontsize=7,
        handlelength=0,
        handletextpad=0,
        columnspacing=1,
        borderpad=0,
        frameon=False
    )
    for text in leg.get_texts():
        plt.setp(text, color=colors[text.get_text()])

    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.set_xlim([0, 2.5])
    ax.set_ylim(type_ylim[atype])
    ax.tick_params(axis='both', labelsize=8)

    atype = 'A/Sa' if atype == 'A' else atype
    atype = 'Ch/Cgh' if atype == 'Ch' else atype
    ax.annotate(
        f"{atype} ({data[0].asteroidfilename})",
        xy=(0.5, 0.95),
        xycoords='axes fraction',
        ha='center',
        va='center',
        fontsize=8
    )

    return


def run():
    data = query()

    fig = plt.figure(figsize=(14, 8))

    outer_axis = fig.add_subplot(1, 1, 1)
    outer_axis.tick_params(labelcolor='none', which='both', top=False, bottom=False, left=False, right=False)
    [outer_axis.spines[spine].set_visible(False) for spine in ['left', 'top', 'right', 'bottom']]
    outer_axis.set_ylabel('Relative Reflectance')
    outer_axis.set_xlabel('Wavelength (\u03BCm)')

    for asteroid_type, position in type_positions.items():
        make_subplot(fig, [d for d in data if d.type == asteroid_type], position)

    plt.subplots_adjust(left=0.05, top=0.94, right=0.95, bottom=0.08, wspace=0.36, hspace=0.36)
    plt.savefig('grid_of_spectra.png', dpi=300)
    plt.show()


if __name__ == "__main__":
    run()
