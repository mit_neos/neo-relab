"""interactive_inspection.py

python interactiv_inspection.py <results_table_name> [output_filename]

Run through asteroid/meteorite matches for visual inspection. Select good/bad match. Results of are save to
'output_filename'

Parameters:
    results_table_name      specify the reults table to inspect over (required)
    output_filename         specify file to output inspection decisions to [default: matches_<results_table_name>.csv]


Requires:
matplotlib
psycopg2-binary

i.e.
$ pip install matplotlib
$ pip install psycopg2-binary
"""

import sys

import matplotlib.pyplot as plt
import psycopg2.extras
from matplotlib.widgets import Button


NEO_RELAB_DB = 'postgresql://bkgbyrlkylzzba:5d515de5ad982d92b7c9ffdf025974822617c1b7cab14110c1a7c162e5f9b98b@ec2-52-71-153-228.compute-1.amazonaws.com:5432/d8i9lq934pv0va'
QUERY = """
SELECT asteroidfilename,
       type                                                     AS asteroid_type,
       chisq,
       albedo_reflectance                                       AS albedo,
       spectrumid,
       sampleid,
       source,
       type1                                                    AS meteorite_type,
       spectrum_data -> 'asteroid' -> 'wavelength'              AS asteroid_wavelength,
       spectrum_data -> 'asteroid' -> 'reflectance'             AS asteroid_reflectance,
       spectrum_data -> 'meteorite' -> 'wavelength'             AS meteorite_wavelength,
       spectrum_data -> 'meteorite' -> 'reflectance_normalized' AS meteorite_reflectance
FROM neo_relab.temp_final_data
ORDER BY type, asteroidfilename;
"""

# make sure that a results table was specified
if len(sys.argv) < 2:
    print(__doc__)
    print("### HELP")
    print('You must specify the results table to inspect (do not include schema). For example:'
          '\n$\tpython interactive_inspection.py results_other_met\n')
    sys.exit(0)

if sys.argv[1].split('_')[0] != 'results':
    print(__doc__)
    print("### HELP")
    print("First argument must be the results table name")
    print("Expected table name to be of format 'results[run_type]'. For example: 'results_other_met'\n")
    sys.exit(0)

results_table = f'neo_relab.{sys.argv[1]}'
save_filename = f'matches_{sys.argv[1]}.csv'
if len(sys.argv) == 3:
    save_filename = sys.argv[1].split('.')[0] + '.csv'

print(f'Reading from results table [{results_table}]')
print(f'Saving results to [{save_filename}]')


def save_decision(row_data, good_match=False):
    """ Save the inspection window decision to file """
    decision_file.write(f'{row_data.asteroidfilename},{row_data.spectrumid},{row_data.sampleid},{good_match}\n')


def callback(event):
    """ Function that gets called on each button click """

    if event.inaxes == exit_axis:
        """ Close button """
        decision_file.close()
        print('Saving and exiting.')
        sys.exit(0)

    good_match = False  # assume a bad match
    if event.inaxes == good_axis:
        """ Change decision to a good match if 'GOOD' button is selected """
        good_match = True

    # close the current figure so the next one can plot
    plt.close()
    save_decision(asteroid_row, good_match)


if __name__ == "__main__":

    # open a file to save to
    decision_file = open(save_filename, 'w')

    print('Running query to gather data...')
    with psycopg2.connect(NEO_RELAB_DB) as conn:
        with conn.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor) as curs:
            print(f"Gathering data for temp_final_table [neo_relab.fn_final_data('{results_table}')]"
                  f"\n This may take a few minutes...")
            curs.execute(f"SELECT neo_relab.fn_final_data('{results_table}')")
            print('Gathering data for plotting...')
            curs.execute(QUERY)
            data = curs.fetchall()

    print('Inspect plots for good/bad matches...')
    n_data = len(data)
    for i, asteroid_row in enumerate(data, start=1):  # data.index:
        fig, ax = plt.subplots(figsize=(8, 6))

        a_wavel = asteroid_row.asteroid_wavelength
        a_ref = asteroid_row.asteroid_reflectance

        # make the plot
        ax.scatter(a_wavel, a_ref, s=3, c='red',
                   label=f'{asteroid_row.asteroidfilename} ({asteroid_row.asteroid_type})')
        ax.scatter(asteroid_row.meteorite_wavelength, asteroid_row.meteorite_reflectance, s=1, c='black',
                   label=f'{asteroid_row.spectrumid}:{asteroid_row.sampleid} ({asteroid_row.meteorite_type})')

        # add some stats about the match
        ax.text(0.02, 0.98, f'chisq = {asteroid_row.chisq}\nalbedo = {asteroid_row.albedo}\nsource={asteroid_row.source}',
                transform=ax.transAxes, fontsize=10, va='top', ha='left')


        # axes limits
        ax.set_xlim([min(a_wavel) - 0.1, max(a_wavel) + 0.1])
        ax.set_ylim([min(a_ref) - 0.1, max(a_ref) + 0.1])

        # labels
        ax.set_ylabel('Relative Reflectance')
        ax.set_xlabel('Wavelength (microns)')
        ax.legend(bbox_to_anchor=(0, 1, 1, 0), loc="lower center", ncol=2)

        # progress axes
        text_axes = plt.axes([0.22, 0.05, 0.15, 0.075])
        text_axes.text(0, 0.5, f'Match {i}/{n_data}', transform=text_axes.transAxes, fontsize=10, va='center', ha='left')
        text_axes.spines[:].set_visible(False)
        text_axes.set(yticklabels=[], xticklabels=[])  # remove the tick labels
        text_axes.tick_params(left=False, bottom=False)

        # make the buttons
        good_axis = plt.axes([0.7, 0.05, 0.1, 0.075])
        bad_axis = plt.axes([0.81, 0.05, 0.1, 0.075])
        exit_axis = plt.axes([0.05, 0.05, 0.15, 0.075])
        good_button = Button(good_axis, 'Good', color='tab:green')
        bad_button = Button(bad_axis, 'Bad', color='tab:red')
        exit_button = Button(exit_axis, 'Save & Exit', color='gainsboro')

        # tell the buttons what to do when clicked
        good_button.on_clicked(callback)
        bad_button.on_clicked(callback)
        exit_button.on_clicked(callback)

        plt.subplots_adjust(bottom=0.2)
        plt.show()

    # save and close the decision file
    decision_file.close()
    print('Done')
