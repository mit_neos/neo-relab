import re
import pandas
import numpy
import matplotlib.pyplot as plt
from matplotlib import cm


DATA_FILE = 'PieChartDatav2.xlsx'
met_colors = {'L': '#b22121', 'LL': '#8f1a1b', 'H': '#d62728', 'CI': '#1f77b4', 'CK': '#1d6ea7', 'CM': '#1b669a',
              'CO': '#185d8d', 'CR': '#165581', 'CV': '#144c74', 'TL': '#103b5a', 'E': '#9467bd', 'EH': '#704299',
              'EL': '#4a2c66', 'R': '#e377c2', 'Acapuloite+Lodranite': '#2ca02c', 'Aubrite': '#278c27',
              'Brachinite': '#248224', 'HEDs': '#217821', 'HED Eucrite': '#1e6e1e', 'HED Howardite': '#1c641c',
              'Ureilite': '#165016', 'Pallasites': '#694138', 'Almahata Sitta': '#299629', 'Mesosiderite': '#8c564b',
              'Other': 'pink', 'Iron': '#909090'}


def get_colors(unique_asteroids):

    s_asteroids = sorted([s for s in unique_asteroids if re.match('^[SsQqAa]', s)])
    c_asteroids = sorted([c for c in unique_asteroids if re.match('^[CcBb]', c)])
    x_asteroids = sorted([x for x in unique_asteroids if re.match('^[XxDd]', x)])
    k_asteroids = sorted([k for k in unique_asteroids if re.match('^[Kk]', k)])
    l_asteroids = sorted([l for l in unique_asteroids if re.match('^[Ll]', l)])
    v_asteroids = sorted([v for v in unique_asteroids if re.match('^[Vv]', v)])
    other_asteroids = sorted([o for o in unique_asteroids if re.match('[^][SsQqCcBbXxDdKkLlVvAa]', o)])

    s_colors = list(cm.Reds_r(numpy.linspace(0.1, 0.3, len(s_asteroids))))
    c_colors = list(cm.Blues(numpy.linspace(0.5, 0.8, len(c_asteroids))))
    # x_colors = list(cm.Greens_r(numpy.linspace(0.1, 0.9, len(x_asteroids))))
    x_colors = list(cm.Purples(numpy.linspace(0.7, 0.9, len(x_asteroids))))
    k_colors = list(cm.Greys(numpy.linspace(0.4, 0.6, len(k_asteroids))))
    # l_colors = list(cm.Purples(numpy.linspace(0.7, 0.8, len(l_asteroids))))
    l_colors = list(cm.Oranges(numpy.linspace(0.5, 0.8, len(l_asteroids))))
    # v_colors = list(cm.YlOrBr(numpy.linspace(0.5, 0.6, len(v_asteroids))))
    v_colors = list(cm.Greens_r(numpy.linspace(0.2, 1, len(x_asteroids))))
    other_colors = list(cm.Pastel1(numpy.linspace(0.1, 0.4, len(other_asteroids))))
    asteroid_colors = dict(
        zip(s_asteroids + c_asteroids + x_asteroids + k_asteroids + l_asteroids + v_asteroids + other_asteroids,
            s_colors + c_colors + x_colors + k_colors + l_colors + v_colors + other_colors))

    print(asteroid_colors)
    return asteroid_colors


def gather_data():

    meteorite_falls = pandas.read_excel(DATA_FILE, usecols=['Meteorite Type', 'Meteorite Falls']).sort_values(by=['Meteorite Falls'])
    debiased_asteroids = pandas.read_excel(DATA_FILE, usecols=['Asteroid Type', 'NEOs (200m - 5km)']).dropna().sort_values(by=['NEOs (200m - 5km)'])
    mbas = pandas.read_excel(DATA_FILE, usecols=['Asteroid Type', 'MBAs (7-9)']).dropna().sort_values(by=['MBAs (7-9)'])
    mbas_minus_4 = pandas.read_excel(DATA_FILE, usecols=['Asteroid Type 2', 'Mass of Main Belt (excluding largest 4)']).dropna().sort_values(by=['Mass of Main Belt (excluding largest 4)'])
    mbas_all = pandas.read_excel(DATA_FILE, usecols=['Asteroid Type 2', 'Mass of Main Belt (all)']).dropna().sort_values(by=['Mass of Main Belt (all)'])

    return meteorite_falls, debiased_asteroids, mbas, mbas_minus_4, mbas_all


def make_plots(data):

    unique_asteroids = data[1]['Asteroid Type'].unique().tolist()
    asteroid_colors = get_colors(unique_asteroids)

    fig = plt.figure(figsize=(20, 6))
    titles = ["Meteorite Falls", "Bias-Corrected NEOs", "MBAs 7-9km", 'Mass of MBAs\n(excluding largest 4)', 'Mass of MBAs\n(all)']
    for i, d in enumerate(data, start=1):

        columns = d.columns.tolist()
        _labels = d[f'{columns[0]}'].tolist()
        _sizes = d[f'{columns[1]}'].tolist()

        color_picker = met_colors if i == 1 else asteroid_colors
        colors = [color_picker[ast] for ast in _labels]

        labels, sizes = [], []
        percent_cutoff = 3
        for l, s in list(zip(_labels, _sizes)):
            labels.append(('', l)[s*100 > percent_cutoff])
            sizes.append(s)

        explode = numpy.linspace(0.2, 0, len(sizes))

        ax = fig.add_subplot(1, 5, i)
        ax.pie(sizes,
               labels=labels,
               startangle=270,
               explode=explode,
               normalize=False,
               autopct=lambda pct: f'{pct:1.1f}%' if pct > percent_cutoff else '',
               textprops={'fontSize': 16},
               radius=1.0,
               colors=colors)
        ax.set_title(titles[i-1] + '\n', size=20)
        ax.axis('equal')

    plt.subplots_adjust(left=0.02, right=0.98, wspace=0.05, bottom=0.05, top=0.805)
    plt.savefig('pie_chart.png', dpi=300)
    plt.show()


if __name__ == "__main__":
    make_plots(gather_data())