import psycopg2
import pandas
import matplotlib.pyplot as plt
from scipy import interpolate

import numpy

import seaborn as sns

sns.set_context('paper')


ASTEROID_DATA_DIR = '/Users/brian/Work/mit/neo-relab/smass/filesToRun/'
RELAB_DATA_DIR = '/Users/brian/Work/mit/neo-relab/relab/data/data/'
NEO_RELAB_DB = 'postgres://bkgbyrlkylzzba:5d515de5ad982d92b7c9ffdf025974822617c1b7cab14110c1a7c162e5f9b98b@ec2-52-71-153-228.compute-1.amazonaws.com:5432/d8i9lq934pv0va'

QUERY_1 = """
WITH temp_rank AS (
    SELECT asteroidfilename,
           spectrumid,
           RANK() OVER (PARTITION BY asteroidfilename ORDER BY chisq) AS rank
    FROM neo_relab.results),
     temp_nspec AS (
         SELECT "Source"                                AS source,
                "GeneralType1"                          AS generaltype1,
                "Type1"                                 AS type1,
                COUNT(DISTINCT "SpectrumID")            AS nspec,
                COUNT(DISTINCT LEFT(sp."SampleID", 10)) AS nsample
         FROM neo_relab.spectra_catalog sp
                  LEFT OUTER JOIN neo_relab.sample_catalog sc
                                  ON sc."SampleID" = sp."SampleID"
         WHERE "Start" <= 900
           AND "Stop" >= 2450
         GROUP BY "Source", "GeneralType1", "Type1"
         ORDER BY COUNT(DISTINCT LEFT(sp."SampleID", 10)) DESC
     )
SELECT m.asteroidfilename,
       m.asteroid,
       m.type,
       m.sampleid,
       m.spectrumid,
       m.type1,
       m.subtype,
       m.chisq,
       m.relab_data_path,
       m.cleaned_subtype
FROM neo_relab.results m
         JOIN temp_rank r ON r.spectrumid = m.spectrumid AND r.asteroidfilename = m.asteroidfilename
         LEFT OUTER JOIN temp_nspec n ON n.source = m.source AND n.generaltype1 = m.generaltype1 AND n.type1 = m.type1
WHERE (type ILIKE 'S%' OR type ILIKE 'Q%')
  AND m.source = 'Other-Met'
  AND m.generaltype1 = 'Rock'
  AND m.type1 = 'Carbonaceous Chondrite'
  AND snr_2micron > 30
  AND rank < 15
  AND m.asteroidfilename NOT IN
      ('a000433.sp245n2.txt', 'a000433.sp256n2.txt', 'a002059.sp257.txt', 'a006456.visnir.txt', 'a013551.sp215.txt',
       'a085818.visnir.txt', 'a143487.nir.txt', 'a217807.visnir.txt', 'a226514.sp225.txt', 'a267729.nir.txt',
       'a303959.sp216.txt',
       'a337866.sp215.txt')
  AND m.asteroidfilename NOT IN
      ('a477885.sp267.txt', 'au2013PJ10.nir.txt', 'au2017ae5.sp263n2.txt', 'a409995.nir.txt', 'a409995.dm19n1.txt',
       'a162510.sp216.txt', 'au2005JR5.nir.txt', 'au2013ux14.sp272.txt')
  AND m.spectrumid IN ('C2LM12', 'C1MT47', 'C1MC05', 'C1MT17', 'BKR1MT315', 'C1TM06', 'CAMS40', 'C2MT17')
ORDER BY m.asteroidfilename, chisq;
"""


def run():

    (ct1, ct1_l) = do_queries(QUERY_1)

    fig, axes = plt.subplots(3, 3, figsize=(9, 10), sharex=True, sharey=True)
    for i, match in enumerate(ct1.match.unique().tolist()):
        this_match = ct1[ct1.match == match]
        axis = axes[i // 3, i % 3]
        axis.scatter(this_match.wavelength, this_match.asteroid_reflectance)
        axis.plot(this_match.wavelength, this_match.meteorite_reflectance, color='k')

        label = ct1_l[match]
        axis.annotate(
            f"{label['asteroid_name']} ({label['asteroid_type']})\n{label['meteorite_spectrum']} ({label['meteorite_type']})",
            xy=(0.4, 1.5), fontsize=9)

        axis.spines['right'].set_visible(False)
        axis.spines['top'].set_visible(False)

        if i % 3 != 0:
            axis.spines['left'].set_visible(False)
            axis.yaxis.set_ticks_position('none')

        if i // 3 != 2:
            axis.spines['bottom'].set_visible(False)
            axis.xaxis.set_ticks_position('none')

    plt.tight_layout()

    fig.text(0.5, 0.01, 'Wavelength ($\mu$m)', ha='center')
    fig.text(0.02, 0.5, 'Relative Reflectance', va='center', ha='center', rotation='vertical')
    fig.subplots_adjust(wspace=0, hspace=0, left=0.08, right=0.99, bottom=0.05, top=0.99)

    plt.xlim([0.25, 2.8])
    plt.ylim([0.3, 1.6])

    plt.savefig('/Users/brian/Desktop/Stypes_CC_matches.png', dpi=300)
    plt.show()


def do_queries(query):
    data = gather_results(query)
    all_data = {'match': [],
                'wavelength': [],
                'asteroid_reflectance': [],
                'meteorite_reflectance': []}
    label_data = {}

    for i, d in enumerate(data):
        a_data = read_asteroid_file(d['asteroidfilename'])
        norm = a_data.loc[(a_data['wavelength'] - 1).abs().argsort()[:1]]
        a_norm_value = norm.reflectance.values[0]
        norm_index = norm.index[0]
        a_data['reflectance'] = a_data.reflectance / a_norm_value

        # shift spectra for plotting
        this_wavelengths = list(a_data.wavelength)
        all_data['wavelength'].extend(this_wavelengths)

        match_name = f"{d['asteroidfilename']}-{d['spectrumid']}"
        all_data['match'].extend([match_name] * len(a_data.wavelength))

        r_data = read_relab_file(d['relab_data_path'])
        f = interpolate.interp1d(r_data.wavelength, r_data.reflectance, kind='cubic')
        m_ref = list(f(a_data.wavelength))

        # doge the spectra purely for plotting
        all_data['asteroid_reflectance'].extend(list(a_data.reflectance))
        all_data['meteorite_reflectance'].extend([(m / m_ref[norm_index]) for m in m_ref])

        label_data[match_name] = {'asteroid_name': d['asteroid'],
                                  'asteroid_type': d['type'],
                                  'meteorite_spectrum': d['spectrumid'],
                                  'meteorite_type': d['cleaned_subtype']}

    return pandas.DataFrame(all_data), label_data


def gather_results(query):
    # grab the data
    connection = psycopg2.connect(NEO_RELAB_DB)
    # data = pandas.read_sql("SELECT * FROM neo_relab.meteorites_per_asteroid;", connection)
    data = pandas.read_sql(query, connection).to_dict('records')
    connection.close()
    return data


def read_asteroid_file(a_file):
    data = pandas.read_csv(f'{ASTEROID_DATA_DIR}{a_file}', delimiter='\s+', usecols=[0, 1],
                           names=['wavelength', 'reflectance'])
    data.sort_values(by=['wavelength'], inplace=True)  # make sure they are sorted propery
    data = data[data.reflectance > 0]
    return data


def read_relab_file(r_file):
    with open(f"{RELAB_DATA_DIR}{r_file}", 'r') as r_data:
        try:
            data = pandas.read_csv(r_data, delimiter='\t', header=1, dtype=numpy.float64, usecols=[0, 1])
            data.rename(columns={'Wavelength(micron)': 'wavelength', 'Reflectance': 'reflectance'}, inplace=True)

        except (pandas.errors.ParserError, ValueError):
            print('Something went wrong with parsing, try they harder way...')
            r_data.seek(0)
            r_data.readline()  # just need to skip the first two lines
            r_data.readline()
            data = []
            for line in r_data:
                split_line = line.rstrip().split()
                if len(split_line) >= 2:
                    try:
                        data.append([float(split_line[0]), float(split_line[1])])

                    except Exception:
                        print(f'Couldnt parse line [{line.rstrip()}] in file [{r_file}]')
                        pass

            data = pandas.DataFrame(data=data, dtype=numpy.float64).rename(columns={0: 'wavelength',
                                                                                    1: 'reflectance'})
        data.dropna(inplace=True)
        data = data[data['reflectance'] > 0]  # Drop where the reflectance is 0 (probably an error?)
        data.sort_values(by=['wavelength'], inplace=True)  # make sure they are sorted propery

        # drop duplicates -- because of interpolate.iterp1d
        data.drop_duplicates(subset=['wavelength'], keep='first', inplace=True)

        return data


if __name__ == "__main__":
    run()
