import psycopg2.extras
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib import cm
import numpy

plt.rcParams["font.family"] = "sans-serif"
plt.rcParams['font.sans-serif'] = 'Helvetica'

type_def = {'Carbonaceous Chondrite': 'CC', 'Iron': 'I', 'Achondrite': 'AC',
            'Ordinary Chondrite': 'OC', 'Enstatite Chondrite': 'EC', 'Stony Iron': 'SI',
            'Chondrite': 'C', 'Enstatite Achondrite': 'EA', 'Primitive Achondrite': 'PA', 'Impact Melt': 'IM'}

# get consistent colors across all figures
all_colors = list(cm.get_cmap('tab10').colors)
all_colors.append((1, 0.25, 1))
type_colors = dict(zip(type_def.keys(), all_colors))


DIRECTORY_NAME = "UnMatched_Meteorites"
NEO_RELAB_DB = 'postgres://bkgbyrlkylzzba:5d515de5ad982d92b7c9ffdf025974822617c1b7cab14110c1a7c162e5f9b98b@ec2-52-71-153-228.compute-1.amazonaws.com:5432/d8i9lq934pv0va'
QUERY = """
SELECT *
FROM neo_relab.fn_calculate_nspec();

WITH unmatched_mets AS (SELECT n.spectrumid, n.sampleid, n.type1, n.subtype --,n.samplename
                        FROM temp_nspec n
                                 LEFT OUTER JOIN neo_relab.temp_final_data d
                                                 ON d.spectrumid = n.spectrumid AND d.sampleid = n.sampleid
                        WHERE n.source = 'Other-Met'
                          AND n.generaltype1 = 'Rock'
                          AND d.sampleid IS NULL
                        ORDER BY type1, subtype)
SELECT *
FROM (
         SELECT nr.spectrumid,
                nr.sampleid,
                nr.type1,
                nr.subtype,
                nr.spectrum_data -> 'meteorite'                AS meteorite_spectrum,
                ROW_NUMBER() OVER (PARTITION BY nr.spectrumid) AS row_number
         FROM neo_relab.results_other_met nr
                  JOIN unmatched_mets um ON um.spectrumid = nr.spectrumid
         WHERE nr.spectrumid = ANY
               (ARRAY ['C6MB43','C1MB27','C1MB33','C1MT141A','C1MT128B','BKR1MT129A',
                       'BKR1TB114','C1MB19B','C1PH46','C1PH49','CCMT312','CGP108',
                       'MGP116','CGP132','LATB62','C1TB119','C1LM31B','CARM175',
                       'C1PH24','BKR1MT307A'])) r
WHERE r.row_number = 1
ORDER BY r.type1, r.subtype;
"""


def query():
    print('Retrieving data...')
    connection = psycopg2.connect(NEO_RELAB_DB)
    cur = connection.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)
    cur.execute(QUERY),
    data = cur.fetchall()
    cur.close()
    connection.close()

    return data


def group_by_type(data):
    print('Organizing data...')
    unique_groups = list(set([d.type1 for d in data]))
    unique_groups_dict = {}
    for uq in unique_groups:
        unique_groups_dict[uq] = []
        for d in data:
            if d.type1 == uq:
                unique_groups_dict[uq].append(d)

    return unique_groups_dict


def _chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i:i + n]


def _calc_fit(x, y, around=0.55):

    # get the closest wavelength to `normalize_wavelength`
    closest_index, closest_value = min(enumerate(x), key=lambda w: abs(w[1]-around))
    if abs(closest_value - around) > 0.5:
        return None

    closest_index = 10 if closest_index < 10 else closest_index

    # do a first order extrapolation if we are at the very beginning of the meteorite spectrum
    fit_order = 2 if closest_index else 1

    # fit a polynomial around the normalize wavelength (2nd degree)
    fit = numpy.poly1d(numpy.polyfit(x[closest_index - 10:closest_index + 10],
                                     y[closest_index - 10:closest_index + 10], fit_order))
    return fit


def _normalize(wavelength, reflectance, normalization_wavelength=None):

    if not normalization_wavelength:
        normalization_wavelength = 0.55 if min(wavelength) <= 0.55 else 1.215

    # calculate the value at `normalize_wavelength`
    normalization_value = _calc_fit(wavelength, reflectance, normalization_wavelength)(normalization_wavelength)
    normalized_reflectance = [r / normalization_value for r in reflectance]

    return normalized_reflectance, normalization_wavelength


def make_plots(data):
    meteorite_type = data[0].type1

    # 100 spectra per page
    chunks = _chunks(data, 100)

    progress = ''
    with PdfPages(f'{DIRECTORY_NAME}/{meteorite_type}.pdf') as pdf:

        for chunk_data in chunks:
            progress = progress + '.'
            print(f'Making plots for {meteorite_type} {progress}', end='\r')

            fig = plt.figure(figsize=(14, 8))

            # each chunk is a 10 x 10 grid
            for i, spectrum in enumerate(chunk_data, 1):
                flux, _ = _normalize(spectrum.meteorite_spectrum['wavelength'],
                                      spectrum.meteorite_spectrum['reflectance'])
                ax = fig.add_subplot(10, 10, i)
                ax.plot(spectrum.meteorite_spectrum['wavelength'], spectrum.meteorite_spectrum['reflectance'], label=f'{spectrum.spectrumid}\n{spectrum.sampleid}\n{spectrum.subtype}')
                ax.legend(loc='center', handlelength=0, fontsize=6, frameon=False, framealpha=0)

                # formatting
                ax.set_xlim([0, 2.5])
                ax.set_ylim([0, 10])
                ax.spines['top'].set_visible(False)
                ax.spines['right'].set_visible(False)
                ax.spines['left'].set_visible(False)
                ax.tick_params(labelleft=False, labelbottom=False, left=False)

            plt.subplots_adjust(left=0.05, bottom=0.05, right=0.985, top=0.985, hspace=0.25, wspace=0.25)
            pdf.savefig()
            plt.close(fig)

    print('')


def make_final_plot(data):

    fig = plt.figure(figsize=(14, 8))

    plot_types = list(set([s.type1 for s in data]))
    major_ax = fig.add_subplot(111, frameon=False)
    major_ax.set_ylabel('Relative Reflectance\n\n')
    major_ax.set_xlabel('\n\nWavelength (\u03BCm)')
    major_ax.tick_params(labelleft=False, labelbottom=False, left=False, bottom=False)

    # dummy lines for major legend
    for mtype in plot_types:
        major_ax.plot([10, 10], [10, 10], color=type_colors[mtype], label=mtype)
    major_ax.set_ylim([0, 1])
    major_ax.set_xlim([0, 1])
    major_ax.legend(bbox_to_anchor=(0.5, 1.15), loc='upper center', ncol=len(type_colors))

    # each chunk is a 10 x 10 grid
    for i, spectrum in enumerate(data, 1):
        ax = fig.add_subplot(4, 5, i)

        # flux, _ = _normalize(spectrum.meteorite_spectrum['wavelength'],
        #                      spectrum.meteorite_spectrum['reflectance'])
        ax.plot(spectrum.meteorite_spectrum['wavelength'], spectrum.meteorite_spectrum['reflectance'],
                color=type_colors.get(spectrum.type1))
        ax.set_title(f'{spectrum.subtype}\n{spectrum.spectrumid} {spectrum.sampleid}', fontdict={'fontsize': 8})

        # formatting
        ax.set_xlim([0, 2.5])
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        # if (i - 1) % 5:
        #     ax.spines['left'].set_visible(False)

        # first_col = (False, True)[(i-1) % 5 == 0]
        ax.tick_params(labelbottom=(False, True)[i > 15])

    plt.subplots_adjust(left=0.04, bottom=0.075, right=0.985, top=0.825, hspace=0.6, wspace=0.25)
    plt.suptitle('Meteorites With No Spectrally Similar Asteroids')
    plt.savefig('meteorite_spectra_that_dont_match.png', dpi=300)
    plt.show()
    plt.close(fig)


if __name__ == "__main__":
    # data = group_by_type(query())
    #
    # os.makedirs(DIRECTORY_NAME, exist_ok=True)
    # for type1, data in data.items():
    #     make_plots(data)

    make_final_plot(query())
