import psycopg2
import pandas
from statistics import median

NEO_RELAB_DB = 'postgres://bkgbyrlkylzzba:5d515de5ad982d92b7c9ffdf025974822617c1b7cab14110c1a7c162e5f9b98b@ec2-52-71-153-228.compute-1.amazonaws.com:5432/d8i9lq934pv0va'


QUERY = """
SELECT DISTINCT asteroidfilename,
                spectrum_data -> 'asteroid' -> 'wavelength' as wavelength,
                spectrum_data -> 'asteroid' -> 'reflectance' AS reflectance,
                spectrum_data -> 'asteroid' -> 'error' AS error
FROM neo_relab.results;"""


def run():

    # get the spectra
    connection = psycopg2.connect(NEO_RELAB_DB)
    data = pandas.read_sql_query(QUERY, connection).to_dict('records')

    snr = []

    n_spectra = len(data)
    with connection.cursor() as cur:
        for i, spectrum in enumerate(data):

            zipped_data = sorted(zip(spectrum['wavelength'], spectrum['reflectance'], spectrum['error']))

            # snr_1micron
            try:
                wavel, sorted_ref_1, sorted_err_1 = zip(*[s for s in zipped_data if 0.95 <= s[0] <= 1.0])
                spectrum['snr_1micron'] = median(sorted_ref_1) / median(sorted_err_1)
            except (ZeroDivisionError, ValueError):
                spectrum['snr_1micron'] = None

            # snr_2micron
            try:
                wavel, sorted_ref_2, sorted_err_2 = zip(*[s for s in zipped_data if 2.1 <= s[0] <= 2.2])
                spectrum['snr_2micron'] = median(sorted_ref_2) / median(sorted_err_2)
            except (ZeroDivisionError, ValueError):
                spectrum['snr_2micron'] = None

            # cur.execute('UPDATE neo_relab.results SET snr_1micron = %s, snr_2micron = %s WHERE asteroidfilename = %s',
            #             (spectrum['snr_1micron'], spectrum['snr_2micron'], spectrum['asteroidfilename']))

            snr.append(dict(asteroidfilename=spectrum['asteroidfilename'],
                            snr_1micron=spectrum['snr_1micron'],
                            snr2micron=spectrum['snr_2micron']))

            print(f'[{i}/{n_spectra}] done', end='\r')

    connection.rollback()

    snr_df = pandas.DataFrame(snr)
    snr_df.to_csv('snr_determinations.csv')


if __name__ == '__main__':
    run()
