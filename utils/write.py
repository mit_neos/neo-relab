import pandas
import json
import psycopg2


def data_to_df(results_data):
    all_rows = []

    for asteroid in results_data:

        asteroid0 = asteroid[0]
        asteroid_name = asteroid0['asteroid_name']
        asteroid_type = asteroid0['type']
        asteroid_file_name = asteroid0['asteroid_file_name']
        snr_1micron = asteroid0['snr_1micron']
        snr_2micron = asteroid0['snr_2micron']
        meteorite_ranks = asteroid[1]
        asteroid_spectrum = asteroid0['spectrum']

        for meteorite in meteorite_ranks:
            wavelengths = list(meteorite.meteorite_wavelength)
            min_wavelength = min(wavelengths)
            max_wavelength = max(wavelengths)

            rank_row = {'AsteroidFileName': asteroid_file_name,
                        'Asteroid': asteroid_name,
                        'Type': asteroid_type,
                        'SNR_1micron': snr_1micron,
                        'SNR_2micron': snr_2micron,
                        'chisq': meteorite.chisq,
                        'p_value': meteorite.p_value,
                        'albedo_reflectance': meteorite.albedo_reflectance,
                        'OverlapStart_microns': min_wavelength,
                        'OverlapStop_microns': max_wavelength,
                        'spectrum_data': json.dumps({
                            'asteroid': {'wavelength_original': asteroid_spectrum['wavelength'].tolist(),
                                         'reflectance_original': asteroid_spectrum['reflectance'].tolist(),
                                         'reflectance': list(meteorite.asteroid_flux_normalized),
                                         'wavelength': list(meteorite.wavelengths),
                                         'error': asteroid_spectrum['error'].tolist()},
                            'meteorite': {'wavelength': wavelengths,
                                          'reflectance': list(meteorite.meteorite_flux),
                                          'reflectance_normalized': list(meteorite.meteorite_flux_normalized)},
                            'removed_slopes': meteorite.removed_slopes
                        })}
            rank_row.update(meteorite.spectra_catalog_data)
            rank_row.update(meteorite.sample_catalog_data)

            all_rows.append(rank_row)

    df = pandas.DataFrame(all_rows)
    df.columns = map(str.lower, df.columns)

    return df


def write_to_database(asteroid_dataframe, connection_string, table_predicate=''):
    conn = psycopg2.connect(connection_string)

    asteroid_filename = asteroid_dataframe.asteroidfilename.unique()[0]
    # print(f'Writing to database asteroid: [{asteroid_filename}]')

    # DROP all rows that have the asteroidfilename to be inserted
    with conn:
        with conn.cursor() as cur:
            cur.execute(f'DELETE FROM neo_relab.results{table_predicate} WHERE asteroidfilename=%s', (asteroid_filename, ))

    conn.close()

    # insert the new/updated data
    asteroid_dataframe.to_sql(f'results{table_predicate}', connection_string, schema='neo_relab', method='multi', if_exists='append', index=False)


def make_results_excel_sheet(results_dataframe):
    print('Generating final excel sheet...')
    results_dataframe.to_excel('./results/Results.xlsx', index=False, freeze_panes=(1, 6))
